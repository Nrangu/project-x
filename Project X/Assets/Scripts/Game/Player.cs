﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

    public class Player : MonoBehaviour
    {
        #region private fields
        [SerializeField] private MobileController _mobileController;
        [SerializeField] private float _speedMove = 3;
        private CharacterController _characterController;
        private Vector3 _moveVector;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            _characterController = GetComponent<CharacterController>();
        }

        // Update is called once per frame
        void Update()
        {
            Move();
        }

        private void Move()
        {
            _moveVector = Vector3.zero;
            _moveVector.x = _mobileController.Horizontal() * _speedMove;
            _moveVector.z = _mobileController.Vertical() * _speedMove;

            if(Vector3.Angle(Vector3.forward,_moveVector) > 1f|| Vector3.Angle(Vector3.forward, _moveVector) == 0)
            {
                Vector3 direct = Vector3.RotateTowards(transform.forward, _moveVector, _speedMove, 0.0f);
                transform.rotation = Quaternion.LookRotation(direct);
            }

            _characterController.Move(_moveVector * Time.deltaTime);
        }
        #endregion
    }
}