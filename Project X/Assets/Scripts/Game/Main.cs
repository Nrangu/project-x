﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


namespace Game
{
    public class Main : MonoBehaviour
    {
        #region private fields
        static Main _ref;
        #endregion

        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;

            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);

 



        }
      


        void Init()
        {
           // GameObject delayBetweenWaves = FindObjectOfType<DelayBetweenWaves>().gameObject;
 
            

        }
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
  
        }
 
        private void OnEnable()
        {
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }

        private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            Init();

        }

        #endregion
        #region public methods
 
        #endregion
        #region public properties
        #endregion
        #region public methods
        static public Main Ref()
        {
            return _ref;
        }
        #endregion

    }
}
