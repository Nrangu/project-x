﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MobileController : MonoBehaviour, IDragHandler,IPointerUpHandler, IPointerDownHandler
{

    #region private fields
    [SerializeField] private Image _joysctickBG;
    [SerializeField] private Image _joysctick;

    private Vector2 _inputVector;

    #endregion
    #region public methods
    public void OnDrag(PointerEventData eventData_)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_joysctickBG.rectTransform, eventData_.position, eventData_.pressEventCamera, out pos))
        {
            pos.x = (pos.x / _joysctickBG.rectTransform.sizeDelta.x);
            pos.y = (pos.y / _joysctickBG.rectTransform.sizeDelta.y);

            _inputVector = new Vector2(pos.x * 2 , pos.y * 2 );
            _inputVector = (_inputVector.magnitude > 1.0f) ? _inputVector.normalized : _inputVector;

            _joysctick.rectTransform.anchoredPosition = new Vector2(_inputVector.x * (_joysctickBG.rectTransform.sizeDelta.x / 2), _inputVector.y * (_joysctickBG.rectTransform.sizeDelta.y / 2));
        }
    }

    public void OnPointerDown(PointerEventData eventData_)
    {
        OnDrag(eventData_);
    }

    public void OnPointerUp(PointerEventData eventData_)
    {
        _inputVector = Vector2.zero;
        _joysctick.rectTransform.anchoredPosition = Vector2.zero;
    }


    public float Horizontal()
    {
        if (_inputVector.x != 0) return _inputVector.x;
        return 0;
    }

    public float Vertical()
    {
        if (_inputVector.y != 0) return _inputVector.y;
        return 0;
    }
    #endregion
    #region private methods
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}
